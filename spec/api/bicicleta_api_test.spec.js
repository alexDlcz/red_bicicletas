var mongoose=require('mongoose');
var Bicicleta=require('../../models/bicicleta');
var request=require('request');


var base_url='http://localhost:3000/api/bicicletas';

describe('BICICLETA API',()=>{
    beforeEach(function (done) {
        mongoDB='mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{ useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex:true  });
        
        const db=mongoose.connection;
        db.on('error', console.error.bind(console,'connection error.'));
        db.once('open', function () {
            console.log('conectado a la base de datos');
            done();
        });       
    });
    
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if(err)console.log(err);
            done();
        });
    });

    describe("GET BICICLETA /",()=> {
        it('status 200', (done)=> {
            request.get(base_url, function (error, response, body) {
                var result= JSON.parse(body);
                expect(response.statusCode).toBe(200); 
               
                done();
            })
        })
     })
    describe('post bicicleta /create',()=>{
        it('status 200',(done)=>{
        var headers={'content-type':'application/json' };
        var aBici='{"code":1,"color":"blue","modelo":"urbana","Lat":20,"lgn":40}'
        request.post({
        headers:headers,
        url: base_url + '/create',
        body: aBici
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            var bici= JSON.parse(body).bicicleta;
            console.log(bici);
            expect(bici.color).toBe("blue");
            expect(bici.ubicacion[0]).toBe(20);
            expect(bici.ubicacion[1]).toBe(40);
            done();
        })
       })
    })
})
         












// var request = require('request');
// var Bicicleta = require('../../models/bicicleta');


// describe('Bicicleta Api',()=>{
//     describe('Bicicleta Get',()=>{
//         it('Status 200',()=> {
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var a= new Bicicleta(1,'verde','urbana',[11.007332, -74.816089]);
//             Bicicleta.add(a);

//             request.get('http://localhost:3000/api/bicicletas', function (error,response,body) {
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });

//     describe('Post Bicicleta/create ',()=>{
//         it('Status 200',(done)=> {
//             var headers= {'content-type':'application/json'}
//             var abici=  '{"id":10,"color":"verde","modelo":"urbana","lat":11.007332,"lng": -74.816089}';
//             request.post({
//                 headers:headers,
//                 url:'http://localhost:3000/api/bicicletas/create',
//                 body: abici
//             },function (error,response,body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe('verde');
//                 done();
//             })
//         });
//     });
//     describe('Post Bicicleta/create ',()=>{
//         it('Status 200',(done)=> {
//             var headers= {'content-type':'application/json'}
//             var abici=  '{"id":10,"color":"verde","modelo":"urbana","lat":11.007332,"lng": -74.816089}';
//             request.post({
//                 headers:headers,
//                 url:'http://localhost:3000/api/bicicletas/create',
//                 body: abici
//             },function (error,response,body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe('verde');
//                 done();
//             });
//         });
//     });
    
// })