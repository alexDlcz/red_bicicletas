var mongoose=require('mongoose');
var Bicicleta=require('../../models/bicicleta');

describe('Testin Bicicletas', function () {
    beforeAll(function(done) {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if(err) console.log(err);
            done();
        });
    });
    describe('bicicleta createInstance', ()=> {
        it('crea una instacia de la bicicleta',()=>{
            var bici =Bicicleta.createInstance(1,"verde", "urbano",[10,-74] )  
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbano");
            expect(bici.ubicacion[0]).toEqual(10);
            expect(bici.ubicacion[1]).toEqual(-74);
        });
    });
    describe('bicicleta allbicis', ()=> {
        it('comienza vacia',(done)=> {
          Bicicleta.allBicis(function (err, bicis) {
              expect(bicis.length).toBe(0)
              done();         
            });
        });
    });
    describe('Bicicleta add', ()=> {
        it('agregando una bici', (done)=> {
            var aBici =new Bicicleta({code: 1,color:"verde",modelo:"urbano"});
            Bicicleta.add(aBici,function (err, newBici) {
                if(err)console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1),
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });
    describe('Bicicleta finById',()=>{
        it('buscando un id',(done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                
                var aBici= new Bicicleta({code:1, color:"blue",modelo:"urbana"});
                Bicicleta.add(aBici,function (err,newBici) {
                    if(err)console.log(err);
                    
                    var aBici2=new Bicicleta({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if(err)console.log(err);
                        Bicicleta.findByCode(2, function (error, targetBici) {
                            console.log(targetBici);
                            
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicletas delete',()=>{
        it('eliminando un id',(done)=>{
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                
                var aBici= new Bicicleta({code:1, color:"blue",modelo:"urbana"});
                Bicicleta.add(aBici,function (err,newBici) {
                    if(err)console.log(err);
                    
                    var aBici2=new Bicicleta({code:2, color:"verde", modelo:"urbana"});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if(err)console.log(err);
                        Bicicleta.findByCode(2, function (error, targetBici) {
                            console.log(targetBici);
                            
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);
                            
                            Bicicleta.findByCode(1, function (error, targetBici1) {
                            console.log(targetBici1);
                            
                                Bicicleta.removeByCode(1, function(err,removecode){
                                    
                                
                                    if(err)console.log(err);
                               
                                console.log(removecode);
                               
                                 
                            done();
                            
                        }) 
                            })
                            });
                        
                    });
                });
            })
        })
    });
});

/*
beforeEach(() => {Bicicleta.allBicis=[]; })
describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('agregamos una', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1, 'negro', 'urbana', [11.007332, -74.816089]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe('Bicicleta.findById', () => {
  it('debe devolver una bici con el id', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var aBici = new Bicicleta(1, 'verde', 'montana');
    var aBici2 = new Bicicleta(2, 'azul', 'pequena');

    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);

    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);

  });
});*/

