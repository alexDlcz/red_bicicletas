var express = require('express');
var router = express.Router();
var bicicletaControler=require('../../controllers/api/bicicletaControllerApi');
const Bicicleta = require('../../models/bicicleta');

router.get('/', bicicletaControler.bicicleta_list);
router.post('/create', bicicletaControler.bicicleta_create);
router.post('/update', bicicletaControler.bicicleta_update);
router.delete('/delete', bicicletaControler.bicicleta_delete);



module.exports=router;